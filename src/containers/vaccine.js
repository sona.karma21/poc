import React, { Component } from 'react';
import { StyleSheet, View,TextInput} from 'react-native';

class Vaccine extends Component {
  
  static navigationOptions = {
    title: 'Vaccine',
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          placeholder = 'Enter Baby Name'
        />
      </View>
      );
    }
  }

  export default Vaccine;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    loginInput: {
      flexDirection : 'row',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 5,
      height: 50,
      borderColor: 'white',
      borderWidth: 1,
      color: 'gray',
      backgroundColor:'white',
      fontSize : 18,
      paddingLeft:10,
      alignItems: 'stretch',
    }
  })
