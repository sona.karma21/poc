import React, { Component } from 'react';
import { StyleSheet, View, Text ,Image,TextInput,TouchableHighlight} from 'react-native';
import Logo from '../components/common/tw.png';

class SignUpOtp extends Component {

  static navigationOptions = {
    title: 'Verification',
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <View style={styles.logoStyle}>
          <Image source={Logo} style={styles.logo} />
        </View>

        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          keyboardType = 'numeric'
          maxLength = {4}
          placeholder = 'Enter OTP'
        />

        <TouchableHighlight underlayColor='#C0C0C0' onPress={() => {navigate('Vaccine')} }
          style = {styles.button}>
            <Text style={styles.btnText}>
              VERIFY
            </Text>
        </TouchableHighlight> 
      </View>
      );
    }
  }

  export default SignUpOtp;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    logo:{
      height:100,
      width:100,
    },
    logoStyle :{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    loginInput: {
      flexDirection : 'row',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 5,
      height: 50,
      borderColor: 'white',
      borderWidth: 1,
      color: 'gray',
      backgroundColor:'white',
      fontSize : 18,
      paddingLeft:10,
      alignItems: 'stretch',
    },
    button : {
      backgroundColor: '#D3D3D3',
      margin:20
    },
    btnText: {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: '#0B5351',
    }
  })
