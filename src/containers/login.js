import React, { Component } from 'react';
import { StyleSheet, View, Text ,Image,TextInput,  TouchableHighlight} from 'react-native';
import Logo from '../components/common/tw.png';
import axios from 'axios';

var loginAPI = 'http://13.228.243.98/api/auth/login';

class Login extends Component {
  
  static navigationOptions = {
    title: 'login',
  }

  constructor(props){
    super(props);

    this.state = {
      phnumber : '',
      password : '',
    }
  }

  handleLogin = () =>{

    axios.post(loginAPI, {
        "phone": this.state.phnumber,
        "password": this.state.password,
    })
    .then(function (response) {
      console.log(response, 'response');
      alert('logged in Successfully')
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.logoStyle}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          placeholder = 'Phone'
          keyboardType = 'numeric'
          maxLength = {10}
          value={this.state.phnumber}
          onChangeText={(phnumber) => this.setState({phnumber})}
        />
        
        <TextInput underlineColorAndroid='transparent' secureTextEntry={true}
          style = {styles.loginInput}
          placeholder = 'Password'
          value={this.state.password}
          onChangeText={(password) => this.setState({password})}
        />

        <TouchableHighlight underlayColor='#C0C0C0' onPress={() => this.handleLogin() || navigate('Vaccine')}
          style = {styles.button}>
            <Text style={styles.btnText}>
              LOGIN
            </Text>
        </TouchableHighlight> 
        <TouchableHighlight underlayColor='#C0C0C0' onPress={() => navigate('SignUp') }>
          <Text style={styles.text}>Not a member Yet? Sign Up now.</Text>
        </TouchableHighlight>
      </View>
      );
    }
  }

  export default Login;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    text : {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: 'white',
    },
    logo:{
      height:100,
      width:100,
    },
    logoStyle :{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    loginInput: {
      flexDirection : 'row',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 5,
      height: 50,
      borderColor: 'white',
      borderWidth: 1,
      color: 'gray',
      backgroundColor:'white',
      fontSize : 18,
      paddingLeft:10,
      alignItems: 'stretch',
    },
    button : {
      backgroundColor: '#D3D3D3',
      margin:20
    },
    btnText: {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: '#0B5351',
    }
  })
