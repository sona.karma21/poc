import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text ,Image,TextInput,TouchableHighlight} from 'react-native';
import Logo from '../components/common/tw.png';
import axios from 'axios';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var userAPI = 'http://13.228.243.98/api/users/';

class SignUp extends Component {

  static navigationOptions = {
    title: 'Sign Up',
  }

  constructor(props){
    super(props);

    this.state = {
      fname : '',
      lname : '',
      phnumber : '',
      passwrod : '',
      value1: 0,
      value1Index: 0,
      radio_props : [ {label: 'Male', value: 0 },{label: 'Female', value: 1 }]
    }
  }

  handleSignUp = () =>{

    axios.post(userAPI, {
        "firstName": this.state.fname,
        "lastName": this.state.lname,
        "phone": this.state.phnumber,
        "password": this.state.password,
        "gender" : this.state.radio_props[this.state.value1Index].label,
    })
    .then(function (response) {
      console.log(response);
      alert('Successfully')
    })
    .catch(function (error) {
      console.log(error);
    });
  }
           //navigate('SignUpOtp')

  render() {
    const { navigate } = this.props.navigation;
    return (
        <View style={styles.container}>
          <ScrollView>      
            <View style={styles.logoStyle}>
              <Image source={Logo} style={styles.logo} />
            </View>

            <TextInput underlineColorAndroid='transparent'
              style = {styles.loginInput}
              placeholder = 'First Name'
              value={this.state.fname}
              onChangeText={(fname) => this.setState({fname})}
            />

            <TextInput underlineColorAndroid='transparent'
              style = {styles.loginInput}
              placeholder = 'Last Name'
              value={this.state.lname}
              onChangeText={(lname) => this.setState({lname})}
            />

            <TextInput underlineColorAndroid='transparent'
              style = {styles.loginInput}
              placeholder = 'Phone Number'
              placeholder = 'Phone'
              keyboardType = 'numeric'
              value={this.state.phnumber}
              onChangeText={(phnumber) => this.setState({phnumber})}
            />
            
            <TextInput underlineColorAndroid='transparent' secureTextEntry={true}
              style = {styles.loginInput}
              placeholder = 'Password'
              value={this.state.password}
              onChangeText={(password) => this.setState({password})}
            />

            <View style={{alignItems:'center',justifyContent:'center',}}>
              <RadioForm formHorizontal={true} animation={true} >
                {this.state.radio_props.map((obj, i) => {
                  var onPress = (value, index) => {
                      this.setState({
                        value1: value,
                        value1Index: index
                      })
                    }
                  return (
                    <RadioButton labelHorizontal={true} key={i} >
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={this.state.value1Index === i}
                        onPress={onPress}
                        buttonInnerColor={'#C0C0C0'}
                        buttonOuterColor={this.state.value1Index === i ? 'white' : 'white'}
                        buttonSize={10}
                        buttonStyle={{}}
                        buttonWrapStyle={{marginLeft: 10, marginTop: 15,}}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        labelHorizontal={true}
                        onPress={onPress}
                        labelStyle={{fontSize: 20, color: 'white', marginTop: 15}}
                        labelWrapStyle={{}}
                      />
                    </RadioButton>
                  )
                })}
              </RadioForm>
            </View>
            <TouchableHighlight underlayColor='#C0C0C0' onPress={ () => this.handleSignUp() || navigate('SignUpOtp') }
              style = {styles.button}>
                <Text style={styles.btnText}>
                  SIGN UP
                </Text>
            </TouchableHighlight> 
          </ScrollView>  
        </View>
      );
    }
  }

  export default SignUp;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    logo:{
      height:100,
      width:100,
    },
    logoStyle :{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    loginInput: {
      flexDirection : 'row',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 5,
      height: 50,
      borderColor: 'white',
      borderWidth: 1,
      color: 'gray',
      backgroundColor:'white',
      fontSize : 18,
      paddingLeft:10,
      alignItems: 'stretch',
    },
    button : {
      backgroundColor: '#D3D3D3',
      margin:20
    },
    btnText: {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: '#0B5351',
    }
  })