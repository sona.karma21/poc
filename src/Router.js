import { StackNavigator } from 'react-navigation';

import Main from './components/Main';
import Login from './containers/login';
import SignUp from './containers/signUp';
import SignUpOtp from './containers/signUpOtp';
import Vaccine from './containers/vaccine';

const BaseNavigation = StackNavigator({
    Main: { screen: Main },
    Login : {screen : Login},
    SignUp : {screen : SignUp},
    SignUpOtp : {screen : SignUpOtp},
    Vaccine : {screen : Vaccine},
});

export default BaseNavigation;
