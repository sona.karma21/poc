import React, { Component } from 'react';
import { StyleSheet, View, Text ,Image,TextInput,TouchableHighlight} from 'react-native';
import Logo from './common/logo.png';

class SignUp extends Component {

  static navigationOptions = {
    title: 'Sign Up',
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.logoStyle}>
          <Image source={Logo} style={styles.logo} />
        </View>

        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          placeholder = 'First Name'
        />

        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          placeholder = 'Last Name'
        />

        <TextInput underlineColorAndroid='transparent'
          style = {styles.loginInput}
          placeholder = 'Phone Number'
        />
        
        <TextInput underlineColorAndroid='transparent' secureTextEntry={true}
          style = {styles.loginInput}
          placeholder = 'Password'
        />

        <TouchableHighlight underlayColor='#C0C0C0' onPress={() => navigate('SignUpOtp')}
          style = {styles.button}>
            <Text style={styles.btnText}>
              SIGN UP
            </Text>
        </TouchableHighlight> 
      </View>
      );
    }
  }

  export default SignUp;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    logo:{
      height:100,
      width:100,
    },
    logoStyle :{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    loginInput: {
      flexDirection : 'row',
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 5,
      height: 50,
      borderColor: 'white',
      borderWidth: 1,
      color: 'white',
      backgroundColor:'white',
      fontSize : 18,
      paddingLeft:10,
      alignItems: 'stretch',
    },
    button : {
      backgroundColor: '#D3D3D3',
      margin:20
    },
    btnText: {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: '#0B5351',
    }
  })