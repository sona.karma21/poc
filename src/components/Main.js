import React, { Component } from 'react';
import { StyleSheet, View , Image, Button, Text, TouchableHighlight} from 'react-native';
import Logo from './common/logo.png';
import Logo2 from './common/logo1.png';

class Main extends Component {
  static navigationOptions = {
    title: 'Welcome',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.logoStyle}>
          <Image source={Logo2}/>
        </View>
        <TouchableHighlight underlayColor='#C0C0C0' onPress={() => navigate('Login')}>
          <Text style={styles.text}>click here ....</Text>
        </TouchableHighlight>
      </View>
      );
    }
  }

  export default Main;

  const styles = StyleSheet.create ({
    container: {
      flex:1,
      flexDirection:'column',
      backgroundColor: '#25AE90',
      justifyContent:'center',
    },
    logo:{
      height:150,
      width:150,
    },
    logoStyle :{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
     text : {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: 'white',
    },
  })
